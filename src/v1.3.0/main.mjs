export function setup(ctx) {
	ctx.onCharacterSelectionLoaded(ctx => {
		// debug
		const debugLog = (...msg) => {
			mod.api.SEMI.log(`${id} v10045`, ...msg);
		};

		// variables
		const id = "semi-auto-slayer";
		const name = "SEMI Auto Slayer";

		let config = {
			monster: {},
			enabled: false,
			equipment: true
		};

		const getMonsterState = (monster) => Math.min(Math.max(0, config.monster[monster.id] || 0), STATE_MAX);
		const getMonsterStateByID = (monsterid) => Math.min(Math.max(0, config.monster[monsterid] || 0), STATE_MAX);
		const getMonsterStates = () => { return { ...config.monster } };
		const getStateReferences = () => {
			return {
				STATE_NONE,
				STATE_SKIP,
				STATE_EXTEND
			}
		};

		// equipment
		const areaBypassItems = game.items.filter(item => item.modifiers && item.modifiers.some(mod => mod.modifier.id == 'melvorD:bypassSlayerItems' || mod.modifier.id == 'melvorD:bypassAllSlayerItems'))
			.sort((a, b) => b.sellsFor.quantity - a.sellsFor.quantity);
		let gearSwapped;

		// modal
		const STATE_CLASSES = ["", "disable", "enable"];
		const STATE_ALL_CLASSES = STATE_CLASSES.join(' ');
		const STATE_NONE = 0;
		const STATE_SKIP = 1;
		const STATE_EXTEND = 2;

		const STATE_MAX = 2;

		const showModal = () => {
			updateModal();
			$(`#modal-${id}`).modal('show');
		};

		const injectModal = () => {
			// Overlay Modal
			const scriptModal = mod.api.SEMI.buildModal(id, `${name}`);
			scriptModal.blockContainer.html(`
                <div class="block-content pt-0 font-size-sm">
                    <div class="row semi-grid" style="pointer-events:none;">
                        <div class="col-md-4 col-sm-12 mb-1">
                            <div class="w-100 semi-grid-item">
                                <img src="assets/media/main/question.svg" /> No Preference
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 mb-1">
                            <div class="w-100 semi-grid-item disable">
                                <img src="assets/media/main/question.svg" /> Skip Monster
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 mb-1">
                            <div class="w-100 semi-grid-item enable">
                                <img src="assets/media/main/question.svg" /> Extend Task
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content pt-0 font-size-sm">
                    <div class="pb-4" id="${id}-container"></div>
                </div>
            `);

			$(scriptModal.modal).on('hidden.bs.modal', () => {
				clearContainer();
				storeConfig();
			});

			// Combat Screen Box
			if ($(`#${id}-combat-box`).length) {
				$(`#${id}-combat-box`).remove();
			}
			$('#combat-fight-container-player').find('slayer-task-menu').after(`
            <div id="${id}-combat-box" class="block block-rounded-double bg-combat-inner-dark p-0 pb-2 mb-2">
				<div class="block-header block-header-default bg-dark-bank-block-header px-3 py-1 mb-1">
					<h5 class="font-size-sm font-w600 mb-0"><img class="skill-icon-xxs m-0 mr-1" src="${ctx.getResourceUrl('semi_icon.png')}"> ${name}</h5>
					<div class="block-options">
						<h5 class="font-w600 text-combat-smoke text-right mr-1 mb-0">
							<a class="combat-action pointer-enabled" id="${id}-help"><small>Help</small></a>
						</h5>
					</div>
				</div>
				<div class="row no-gutters px-2">
					<div class="col-12 my-2">
						<button role="button" id="${id}-task-settings" style="" class="btn btn-primary w-100">Slayer Task Settings</button>
					</div>
					<div class="col-12 my-2">
						<div class="custom-control custom-switch custom-control-lg w-100 text-center">
							<input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
							<label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
						</div>
					</div>
					<div class="col-12 my-2">
						<div class="custom-control custom-switch custom-control-sm w-100 text-center">
							<input class="custom-control-input" type="checkbox" name="${id}-manage-equipment" id="${id}-manage-equipment">
							<label class="font-weight-normal ml-2 custom-control-label" for="${id}-manage-equipment">Manage Equipment</label>
						</div>
					</div>
				</div>
			</div>`);

			$(`#${id}-enable-check`).on('change', function (e) {
				toggleEnabledStatus();
				checkSlayerEquipment();
			}).prop('checked', config.enabled);

			$(`#${id}-manage-equipment`).on('change', function (e) {
				toggleEquipmentStatus();
			}).prop('checked', config.equipment);

			$(`#${id}-task-settings`).on('click', function (e) {
				showModal();
			});
			$(`#${id}-help`).on('click', function (e) {
				helpAlert();
			});
		};

		const updateModal = () => {
			clearContainer();
			buildCategories();
		};

		const clearContainer = () => {
			$(`#${id}-container`).empty();
		};

		const toggleEnabledStatus = () => {
			config.enabled = !config.enabled;
			storeConfig();
		};
		const toggleEquipmentStatus = () => {
			config.equipment = !config.equipment;
			storeConfig();
		};

		const buildCategories = () => {
			$(`#${id}-container`).html(game.combat.slayerTask.categories.allObjects.map(category => buildCategory(category)).join(''));

			tippy(`#${id}-container [data-tippy-content]`, {
				animation: false,
				allowHTML: true
			});

			$(`#${id}-container .semi-grid-item`).on('click', function (e) {
				const elm = $(this)
				const monsterid = elm.data("monster");

				elm.removeClass(STATE_ALL_CLASSES);
				config.monster[monsterid] = (config.monster[monsterid] || 0) + 1;

				if (config.monster[monsterid] > STATE_MAX)
					delete config.monster[monsterid];

				elm.addClass(STATE_CLASSES[config.monster[monsterid] || 0]);

			});
		}

		const buildCategory = (category) => {
			const monsterList = getMonsterSelection(category).sort((a, b) => {
				const acl = a.combatLevel;
				const bcl = a.combatLevel;
				return acl == bcl ? a.levels.Hitpoints - b.levels.Hitpoints : acl - bcl;
			});

			return `
            <h2 class="content-heading border-bottom mb-2 pb-2"><img class="skill-icon-xxs m-0 mr-1" src="${category.realm.media}">${category.name}</h2>
            <div class="row">
                <div class="col semi-grid">
                    ${monsterList.map(buildMonsterItem).join('')}
                </div>
            </div>`;
		}

		const buildMonsterItem = (monster) => {
			const STATE = getMonsterState(monster);

			return `<div class="semi-grid-item float ${STATE_CLASSES[STATE]} m-1" data-monster="${monster.id}" data-tippy-content="${monster.name}">
            <img src="${monster.media}" alt="${monster.name}" />
        </div>`;
		};

		const helpAlert = () => {
			SwalLocale.fire({
				title: `${name}`,
				width: '60em',
				html: `<div class="text-left font-size-sm">Auto Slayer works to automate Slayer tasks by either <span class="text-danger">Skipping unwanted Task</span>, <span class="text-success">Extended Tasks</span>, or having <span class="text-warning">No Preference</span> either way.

                You can setup how you want monsters to be handle by opening the selection screen using <span class="text-info">Slayer Task Settings</span> and clicking the monster icons to cycle between the 3 states.

                For most Slayer Task without any special conditions, it will simply jump to the next Slayer Monster and wait till the task is complete.

                While Auto Slayer is enabled and in combat, it will jump to the current Slayer Task Monster to stay on task unless currently in a Dungeon or an event is active.
                <hr><span class="font-size-base font-w600 text-info">Area Requirements</span>
                Certain Slayer Areas have item requirements that will be handled in the following order:

                <span class="text-info">MI "Automatically fight new Slayer Task"</span>:
                When the In-Game Auto Slayer is enabled, it filters out Slayer Monsters that your current character doesn't meet the requirements for, this is useful if you want to always skip monsters in areas you don't have the gear requirements to save on tokens, or don't want ${name} to swap gear to meet the requirements.

                <span class="text-info">Gear Swapping</span>:
                If a Slayer Monster is selected but gear requirements are not met, then the following gear is equipped from top to bottom until the condition is met, if it is found within the bank:
                ${areaBypassItems.map(item => `- ${item.name}`).join('<br>')}
                - Area Specific Equipment

                As some slayer equipment is equipped in the <span class="text-warning">Off-Hand Slot</span>, <span class="text-info">1H weapons are recommend</span> as it will <span class="text-danger">leave you without a weapon</span> in this event.

                When a Task is completed, swapped gear is restored before starting the next Slayer Task.

				<span class="text-warning">"Manage Equipment" will skip gear swapping and may leave you in danger. This should only be used if you have a way to fulfill the requirements though other means such as a mod.</span>
                <hr><span class="font-size-base font-w600 text-info">Task Skipping</span>
                Is a Monster is set to be skipped, or the requirements to fight the monster cannot be met, then the task is rerolled until it can find a Slayer Task it can complete.

                In the event a task cannot be skipped due to a lack of <span class="text-info">Slayer Coins</span>, combat is ended.
                <hr><span class="font-size-base font-w600 text-info">Task Extending</span>
                If a Monster is set to be Extended and you have enough <span class="text-info">Slayer Coins</span> to extend, then the task will be extended.
                </div>`.replace(/\r\n|\r|\n/gi, "<br>")
			});
		};

		// script
		const getMonsterSelection = (category) => {
			const categoryFilter = category.getMonsterFilter();
			return game.monsters.filter(monster => {
				const monsterArea = game.getMonsterArea(monster);
				return (monster.canSlayer && monsterArea.realm === category.realm && categoryFilter(monster));
			});
		}

		// Do nothing if not enabled, in a event, or in a dungeon.
		const isAllowed = () => {
			return config.enabled && !game.combat.isEventActive && !(game.combat.selectedArea instanceof Dungeon);
		};

		const endCombat = (msg) => {
			if (game.combat.isActive) {
				debugLog(msg);
				notifyPlayer(game.combat, msg, 'danger');
				game.combat.stop();
			}
			else {
				debugLog('Not in Combat...');
			}
		};

		const checkSlayerEquipment = () => {
			if (!isAllowed()) {
				return;
			}

			if (game.combat.selectedMonster != game.combat.slayerTask.monster) {
				debugLog(`Off task, jumping to slayer monster if possible.`);
				if (game.combat.isActive) {
					debugLog('Ending combat off task.');
					game.combat.stop();
				}
				slayerSetTask();
				return;

			}
			else {
				if (!game.combat.player.manager.areaRequirementsMet || !game.combat.isActive) {
					debugLog(`On task, but don't meet requirements or out of combat, handling...`);
					slayerSetTask();
				}
			}
		};

		const slayerSetTask = () => {
			if (!isAllowed() || game.combat.slayerTask.monster == null) {
				return;
			}

			debugLog("--------------------");

			const task = game.combat.slayerTask;
			const monster = task.monster;
			const area = game.getMonsterArea(monster);
			const state = getMonsterState(monster);
			const slayerLevelReq = area instanceof SlayerArea ? area.slayerLevelRequired : 0;
			const slayerCategory = task.category;
			const rollCost = task.getRollCosts(slayerCategory);
			const extendCost = task.getExtensionCosts(slayerCategory);

			let shouldSkipMonster = state == STATE_SKIP;

			// Restore Old Equipemnt
			if (gearSwapped) {
				mod.api.SEMI.equipmentDifferencesRestore(gearSwapped);
				gearSwapped = null;
			}

			// Check if all monsters in tier are skipped to prevent user error.
			const hasFightableMonster = getMonsterSelection(slayerCategory).some(monster => getMonsterState(monster) != STATE_SKIP);
			if (!hasFightableMonster) {
				debugLog(getMonsterSelection(slayerCategory));
				endCombat("All Monsters of this tier are set to skip, ending combat to avoid wasting Slayer Coins.");
				return;
			}

			// Get Unmet Requirements
			let unmetRequirements = [];
			let reqs = area.entryRequirements;
			for (let t = 0; t < reqs.length; t++) {
				let req = reqs[t];

				if (!game.checkRequirement(req, false, slayerLevelReq)) {
					unmetRequirements.push(req);

					if (req.type != "SlayerItem") {
						shouldSkipMonster = true;
						debugLog("Found Unmet Non-Item Requirement");
					}
				}
			}

			debugLog(monster.name, "(", monster.id, ")");
			debugLog("extended", task.extended, "killsLeft", task.killsLeft, "state", state);
			debugLog(area.name, "(", area.id, ")", "canAccess", game.checkRequirements(area.entryRequirements, false, slayerLevelReq));
			debugLog("Unmet:", unmetRequirements);

			// Skip Equipment Requirements if allowed.
			if(!config.equipment)
				unmetRequirements.length = 0;

			// Check Unmet Requirements
			if (unmetRequirements.length > 0 && !shouldSkipMonster) {
				debugLog("Attempting to handle slayer item requirements.");

				let oldEquipment = mod.api.SEMI.equipmentSnapshot();

				for (let r = 0; r < unmetRequirements.length; r++) {
					let req = unmetRequirements[r];
					let requiredItem = req.item;

					let itemEquipPriority = [...areaBypassItems, requiredItem];

					for (let i = 0; i < itemEquipPriority.length; i++) {
						const nextItem = itemEquipPriority[i];
						const oldItem = game.combat.player.equipment.getItemInSlot(nextItem.validSlots[0].id);

						if (game.bank.getQty(nextItem) <= 0)
							continue;

						if (!game.checkRequirements(nextItem.equipRequirements, false))
							continue;

						if (game.combat.player.equipItem(nextItem, game.combat.player.selectedEquipmentSet)) {
							debugLog(`Replaced ${oldItem.name} with ${nextItem.name} to pass requirement.`);
							if (game.checkRequirement(req, false, slayerLevelReq)) {
								debugLog(`Check passed.`);
								unmetRequirements.splice(r, 1);
								break;
							} else {
								debugLog(`Check failed.`);
							}
						}
					}
				}

				gearSwapped = mod.api.SEMI.equipmentDifferences(oldEquipment);
				debugLog(gearSwapped);

				if (unmetRequirements.length > 0) {
					shouldSkipMonster = true;
				}
			}

			if (!shouldSkipMonster) {
				if (state == STATE_EXTEND) {
					if (!game.combat.slayerTask.extended) {
						if (extendCost.checkIfOwned()) {
							debugLog("State: Extending Task");
							task.extendTask();
						}
						else {
							debugLog("State: Extending Task - Cannot Afford");
						}
					}
					else {
						debugLog("State: Extending Task - Already Extended");
					}
				}
				else if (state == STATE_SKIP) {
					debugLog("State: Skip Monster");
					shouldSkipMonster = true;
				}
				else {
					debugLog("State: No Preference");
				}
			}

			// Short Task Hack
			//game.combat.slayerTask.killsLeft = Math.min(1, game.combat.slayerTask.killsLeft);
			//game.combat.slayerTask.renderQueue.task = true;

			if (shouldSkipMonster) {
				debugLog("Skipping Monster...");
				if (game.combat.isActive) {
					debugLog('Ending Combat skipping monster.');
					game.combat.stop();
				}

				if (!rollCost.checkIfOwned()) {
					endCombat("Not enough slayer coins to skip task, leaving combat.");
					return;
				}

				game.combat.slayerTask.selectTask(slayerCategory, true, false, true);
				return;
			}

			// Jump to next Monster if possible.
			if (!shouldSkipMonster) {
				if (game.combat.selectedMonster != game.combat.slayerTask.monster || !game.combat.isActive) {
					debugLog("Jumping to Monster", game.combat.isActive);
					game.combat.selectMonster(monster, area);
				} else {
					debugLog("On Selected Monster");
				}
			}
			else {
				debugLog("Cannot jump to enemy:");
				debugLog("UR:", unmetRequirements.length, "AS:", task.autoSlayer);
			}
		};

		// config
		const storeConfig = () => {
			ctx.characterStorage.setItem('config', config);
		}

		const loadConfig = () => {
			const storedConfig = ctx.characterStorage.getItem('config');
			if (!storedConfig) {
				return;
			}

			config = { ...config, ...storedConfig };
		}

		// hooks + game patches
		ctx.onCharacterLoaded(() => {
			loadConfig();

			ctx.patch(SlayerTask, 'setTask').after(function (result) {
				slayerSetTask();
			});

			ctx.patch(CombatManager, 'spawnEnemy').after(function (result) {
				checkSlayerEquipment();
			});

			injectModal();

            mod.api.SEMI.addSideBarModSetting(name, ctx.getResourceUrl('semi_icon.png'), showModal);
		});

		// api
		ctx.api({
			getStateReferences,
			getMonsterStates,
			getMonsterState,
			getMonsterStateByID,
		});
	});
}
